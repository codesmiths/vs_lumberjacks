
# v0.0.7 - 2021-03-10 - Fixed deposits

## Bugfixes

* Fix issue #30 - deposit generation was broken
  (deep ore depositis and ruins were missing) Swamp water
  deposit generation disabled for now.

## New Features

### New Plants:

* Dense, more bigger ferns
* Hanging moss

### New Trees:

* Mossy Oak (Work in Progress :)

# v0.0.6 - 2021-03-02 - First light

## New Features

The first public release, it adds:

### New Biomes:

* Temporate rain forests

### New Trees:

* *Sequioa*, variants: coastal and giant

### New Plants:

* Moss
* Small ferns

## New Soil Types:

* Mossy block (bare, and very sparsly overgrown)

## New Wood Types:

* Redwood

## New Items:

* Gluepot (raw, backed empty, filled with resin, filled with glue)

## New Recipes:

  * Clayform: gluepot
  * Baking: gluepot
  * Grid: Fill gluepot
  * Cooking: glue
