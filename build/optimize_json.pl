#!/usr/bin/perl -w

use warnings;
use strict;

# Optimize a JSON file by:
# * removing trailing whitespace
# * converting \r\n to \n
#
# Usage: perl build/optimize_json.pl FILE1.json [FILE2.json ... ]

for my $file (@ARGV)
  {
  warn("Cannot read $file: $!"), next unless -f $file;

  out("Reading: $file");
  open my $FH, '<', $file or die("Cannot read $file: $!");

  my $modified = 0;
  my $doc;
  # read input line by line
  while (my $line = <$FH>)
    {
    my $org_line = $line;
    $line =~ s/\r\n/\n/;		# convert \r\n to \n
    $line =~ s/\s+\n/\n/;		# remove trailing whitespace

    # remember all the content
    $doc .= $line;

    # did we modify the file in any way?
    $modified = 1 if $line ne $org_line;
    }
  close $FH;

  if ($modified)
    {
    # make backup?
    out("Contents modified, writing back: $file");
    rename $file, "$file.bak";
    open my $FH, '>', $file or die("Cannot write back to $file: $!");
    print $FH $doc;
    close $FH;
    }
  }

sub out
  {
  my $msg = join("", @_);

  print $msg,"\n";
  }
