This directory exists to workaround the game bug where
certain items forget to set the domain for the texture
and then try to load the texture from "game" instead
of the modid.
