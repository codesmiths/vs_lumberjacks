This mod would have not been possible with the help of countless other people. We stand on the shoulders of giants!

Thank you,

~Tels & Phiwa

# Contributions

## Textures

* Wood textures based on photographs by Paul Wozniak and Eva Sajdak - https://freestocktextures.com

# Thank you

* To Tyron, Saraty and all the other team members for creating such a great game!

* All the people answering (our and others) questions, on the modding-help discord channel or elsewere: You are awesome!
Especially:
  + DArkHekRoMaNT
  + Radfast
