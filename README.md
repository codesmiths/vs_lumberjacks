# Lumberjacks

"I'm a lumberjack, and I'm okay."

  <u>**This is an alpha release and work in progress!**</u>

This Vintage Story mod enhances the game by adding new trees, ferns and moss to create more immersive forests.
It also adds new wood types and items related to wood working.

You can find more information and pretty pictures at our [Wiki](https://gitlab.com/codesmiths/vs_lumberjacks/-/wikis/home).

We hope you enjoy our work.

~Phiwa & Tels

# Download

* The mod From [Official Vintage Story ModDB](https://mods.vintagestory.at/show/mod/226)
* The source code can be found on [Gitlab](https://gitlab.com/codesmiths/vs_lumberjacks/-/releases)

# Installation

This mod changes the world generation - new trees, plants and blocks will only appear
in newly generated chunks. In an existing world, you need to travel to new areas
to find the new things. If in doubt, please create a new world.

  <u>**Make a backup of your savegame and world before trying this mod!**</u>

# Features

* New Biome: **Temperate rain forests**
* Better and more immersive forests containing
  * Redwood trees (variants: *Sequioa* coastal, giant)
  * Moss & mossy soil
  * Small and dense ferns
  * Hanging moss
* New items and recipes:
  * Glue
  * Gluepot (empty, filled with resin or glue)

Redwood can be harvested and used to craft the following blocks/items:

* Boards, planks, plank slabs, stairs and any other wood item that uses boards

# Languages

* English (100%)
* German (100%)

If you would like to translate this mod into other languages, please [contact us](Contact).

# Known issues

* Survival mode:
  * Glue pots on shelves are always oriented west, instead of the direction of the shelf.
  * Glue has no real use yet.
* Creative mode:
  * Redwood trees do not appear in the tree generator dropdown list

Please see also the [Wishlist](Wishlist) for a list of issues we found in the game engine or survival mod itself.

# Changes and Roadmap

Please see the [Changelog](Changelog) for the changes in the latest release.

The full [Roadmap](Roadmap) page contains all the planned or possible features.

If you have any ideas, or know about features that already done in another mod, then we love [to hear from you](Contact).

# Signature key

All our releases are signed using PGP (GnuPG) and their integrity can be verified by using the public key as published
[on the wiki](https://gitlab.com/codesmiths/vs_lumberjacks/-/wikis/Signing_key).

