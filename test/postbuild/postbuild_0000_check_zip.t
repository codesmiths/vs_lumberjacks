#!/usr/bin/perl

# Test that the .zip file and its contents look sane

use Test::More tests =>
	# basic tests
	4 +
	# + 2 tests for each tested file
	2 * 2;
use JSON;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use strict;
use warnings;

sub read_file($);
sub read_modinfo();

# we expect the build stage to put the mod here
use constant DIST_DIR => 'dist';

# check that the right ZIP was generated
my $modinfo = read_modinfo();

my $zipfile = DIST_DIR . '/' . $modinfo->{modid} . '-' . $modinfo->{version} . '.zip';

ok (-f $zipfile, 'Zip file exists');
ok (-s $zipfile > 200, 'Zip file is at least 200 byte');
like (`file -i '$zipfile'`, qr/application\/zip/, 'File is actually a ZIP file');

my $modzip = Archive::Zip->new();
ok ($modzip->read( $zipfile ) == AZ_OK, 'Could open the ZIP file');

# check that certain files are present in the archive
for my $file ( qw/modinfo.json modicon.png/)
  {
  my $member = $modzip->memberNamed( $file );
  # is there a member with the name "$file"?
  is (ref($member), 'Archive::Zip::ZipFileMember', "Archive contains $file");
  # and has it the same uncompressed size as the original file?
  my $fsize = -s $file;
  is ($member->{uncompressedSize}, $fsize, "$file is $fsize bytes");
  }

#############################################################################
# Helper subroutines

sub read_file($)
  {
  my ($file) = @_;

  open (my $FH, '<', $file) or die ("Cannot read $file: $!");
  binmode $FH, ':utf8';
  local $/ = undef;	# slurp mode
  my $data = <$FH>;	# read all data
  close $FH;

  \$data;		# return a reference to the data
  }

sub read_modinfo()
  {
  # read and parse modinfo.json and return it as in-memory hash
  my $json = read_file('modinfo.json');

  my $info;
  eval {
    $info = decode_json($$json);
  };

  die ("Could not decode JSON: $!") unless ref($info);

  $info;
  }

